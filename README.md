# Shinka

A collection of my CSS snippets and modifications for [Obsidian](https://obsidian.md/), under an umbrella of a design language I like to call "shinka".

My Obsidian theme.